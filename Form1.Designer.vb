﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipale
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipale))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboCode = New System.Windows.Forms.ComboBox()
        Me.lstClient = New System.Windows.Forms.ListBox()
        Me.lblModuleName = New System.Windows.Forms.Label()
        Me.lblModuleFW = New System.Windows.Forms.Label()
        Me.cboModulo = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.lblProduct = New System.Windows.Forms.Label()
        Me.cmdReset = New System.Windows.Forms.Button()
        Me.lblSuite = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.FWIdentificator.My.Resources.Resources.ARCAwhite
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(120, 120)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 34.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.LightGray
        Me.Label1.Location = New System.Drawing.Point(298, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(488, 53)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = " FW IDENTIFICATOR"
        '
        'cboCode
        '
        Me.cboCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboCode.BackColor = System.Drawing.SystemColors.Menu
        Me.cboCode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCode.FormattingEnabled = True
        Me.cboCode.Location = New System.Drawing.Point(274, 193)
        Me.cboCode.Name = "cboCode"
        Me.cboCode.Size = New System.Drawing.Size(425, 33)
        Me.cboCode.TabIndex = 3
        '
        'lstClient
        '
        Me.lstClient.BackColor = System.Drawing.SystemColors.Menu
        Me.lstClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!)
        Me.lstClient.FormattingEnabled = True
        Me.lstClient.ItemHeight = 25
        Me.lstClient.Location = New System.Drawing.Point(274, 232)
        Me.lstClient.Name = "lstClient"
        Me.lstClient.Size = New System.Drawing.Size(425, 104)
        Me.lstClient.TabIndex = 4
        '
        'lblModuleName
        '
        Me.lblModuleName.AutoSize = True
        Me.lblModuleName.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.0!, System.Drawing.FontStyle.Bold)
        Me.lblModuleName.ForeColor = System.Drawing.Color.LightGray
        Me.lblModuleName.Location = New System.Drawing.Point(12, 434)
        Me.lblModuleName.Name = "lblModuleName"
        Me.lblModuleName.Size = New System.Drawing.Size(157, 30)
        Me.lblModuleName.TabIndex = 5
        Me.lblModuleName.Text = "CASSETTE"
        '
        'lblModuleFW
        '
        Me.lblModuleFW.AutoSize = True
        Me.lblModuleFW.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.0!, System.Drawing.FontStyle.Bold)
        Me.lblModuleFW.ForeColor = System.Drawing.Color.White
        Me.lblModuleFW.Location = New System.Drawing.Point(12, 477)
        Me.lblModuleFW.Name = "lblModuleFW"
        Me.lblModuleFW.Size = New System.Drawing.Size(138, 32)
        Me.lblModuleFW.TabIndex = 5
        Me.lblModuleFW.Text = "READER"
        '
        'cboModulo
        '
        Me.cboModulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboModulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboModulo.BackColor = System.Drawing.SystemColors.Menu
        Me.cboModulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboModulo.FormattingEnabled = True
        Me.cboModulo.Location = New System.Drawing.Point(274, 154)
        Me.cboModulo.Name = "cboModulo"
        Me.cboModulo.Size = New System.Drawing.Size(425, 33)
        Me.cboModulo.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.LightGray
        Me.Label2.Location = New System.Drawing.Point(123, 151)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 31)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "MODULE:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.LightGray
        Me.Label3.Location = New System.Drawing.Point(11, 190)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(257, 31)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "PRODUCT CODE:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.Color.LightGray
        Me.Label4.Location = New System.Drawing.Point(51, 232)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(217, 31)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "CLIENT NAME:"
        '
        'cmdSearch
        '
        Me.cmdSearch.Enabled = False
        Me.cmdSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.cmdSearch.Location = New System.Drawing.Point(705, 266)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(207, 70)
        Me.cmdSearch.TabIndex = 10
        Me.cmdSearch.Text = "SEARCH"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'lblProduct
        '
        Me.lblProduct.AutoSize = True
        Me.lblProduct.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.0!, System.Drawing.FontStyle.Bold)
        Me.lblProduct.ForeColor = System.Drawing.Color.LightGray
        Me.lblProduct.Location = New System.Drawing.Point(12, 394)
        Me.lblProduct.Name = "lblProduct"
        Me.lblProduct.Size = New System.Drawing.Size(23, 30)
        Me.lblProduct.TabIndex = 11
        Me.lblProduct.Text = "-"
        '
        'cmdReset
        '
        Me.cmdReset.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdReset.Enabled = False
        Me.cmdReset.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold)
        Me.cmdReset.Location = New System.Drawing.Point(705, 154)
        Me.cmdReset.Name = "cmdReset"
        Me.cmdReset.Size = New System.Drawing.Size(207, 70)
        Me.cmdReset.TabIndex = 12
        Me.cmdReset.Text = "RESET"
        Me.cmdReset.UseVisualStyleBackColor = True
        '
        'lblSuite
        '
        Me.lblSuite.AutoSize = True
        Me.lblSuite.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.0!, System.Drawing.FontStyle.Bold)
        Me.lblSuite.ForeColor = System.Drawing.Color.LightGray
        Me.lblSuite.Location = New System.Drawing.Point(207, 353)
        Me.lblSuite.Name = "lblSuite"
        Me.lblSuite.Size = New System.Drawing.Size(23, 30)
        Me.lblSuite.TabIndex = 11
        Me.lblSuite.Text = "-"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 19.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.LightGray
        Me.Label6.Location = New System.Drawing.Point(13, 353)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(188, 30)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "SUITE CODE:"
        '
        'frmPrincipale
        '
        Me.AcceptButton = Me.cmdSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Purple
        Me.CancelButton = Me.cmdReset
        Me.ClientSize = New System.Drawing.Size(1052, 596)
        Me.Controls.Add(Me.cmdReset)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblSuite)
        Me.Controls.Add(Me.lblProduct)
        Me.Controls.Add(Me.cmdSearch)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboModulo)
        Me.Controls.Add(Me.lblModuleFW)
        Me.Controls.Add(Me.lblModuleName)
        Me.Controls.Add(Me.lstClient)
        Me.Controls.Add(Me.cboCode)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmPrincipale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FW Identificator"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboCode As ComboBox
    Friend WithEvents lstClient As ListBox
    Friend WithEvents lblModuleName As Label
    Friend WithEvents lblModuleFW As Label
    Friend WithEvents cboModulo As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cmdSearch As Button
    Friend WithEvents lblProduct As Label
    Friend WithEvents cmdReset As Button
    Friend WithEvents lblSuite As Label
    Friend WithEvents Label6 As Label
End Class
